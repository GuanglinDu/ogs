<?xml version="1.0" encoding="ISO-8859-1"?>
<OpenGeoSysProject>
	<meshes>
		<mesh>disc_with_hole.vtu</mesh>
		<mesh>disc_with_hole_left.vtu</mesh>
		<mesh>disc_with_hole_bottom.vtu</mesh>
		<mesh>disc_with_hole_hole.vtu</mesh>
        <mesh>disc_with_hole_edge.vtu</mesh>
	</meshes>
    <processes>
        <process>
            <name>HM</name>
            <type>HYDRO_MECHANICS</type>
            <integration_order>2</integration_order>
			<dimension>2</dimension>
            <constitutive_relation>
                <type>LinearElasticIsotropic</type>
                <youngs_modulus>E</youngs_modulus>
                <poissons_ratio>nu</poissons_ratio>
            </constitutive_relation>
			<process_variables>
                <displacement>displacement</displacement>
                <pressure>pressure</pressure>
            </process_variables>
			<secondary_variables>
                <secondary_variable internal_name="sigma" output_name="sigma"/>
                <secondary_variable internal_name="epsilon" output_name="epsilon"/>
                <secondary_variable internal_name="velocity" output_name="velocity"/>
            </secondary_variables>
            <specific_body_force>0 0</specific_body_force>
        </process>
    </processes>
	<media>
        <medium>
            <phases>
                <phase>
                    <type>Gas</type>
                    <properties>
                        <property>
                            <name>viscosity</name>
                            <type>Constant</type>
                            <value>1e-3</value>
                        </property>
                        <property>
                            <name>density</name>
                            <type>Constant</type>
                            <value>1</value>
                        </property>
                    </properties>
                </phase>
                <phase>
                    <type>Solid</type>
                    <properties>
                        <property>
                            <name>density</name>
                            <type>Constant</type>
                            <value>2000</value>
                        </property>
                    </properties>
                </phase>
            </phases>
            <properties>
                <property>
                    <name>porosity</name>
                    <type>Constant</type>
                    <value>0.01</value>
                </property>
                <property>
                    <name>biot_coefficient</name>
                    <type>Constant</type>
                    <value>1</value>
                </property>
                <property>
                    <name>reference_temperature</name>
                    <type>Constant</type>
                    <value>293.15</value>
                </property>
                <property>
                    <name>permeability</name>
                    <type>OrthotropicEmbeddedFracturePermeability</type>
                    <intrinsic_permeability>k</intrinsic_permeability>
                    <mean_frac_distances>1e-2 1e-2 1e-2</mean_frac_distances>
                    <threshold_strains>1e-5 1e-5 1e-5</threshold_strains>
                    <fracture_normals> 1 0 0    0 1 0 </fracture_normals>
                    <fracture_rotation_xy>phi</fracture_rotation_xy>
                    <fracture_rotation_yz>zero</fracture_rotation_yz>
                </property>
            </properties>
        </medium>
    </media>
    <time_loop>
        <processes>
            <process ref="HM">
                <nonlinear_solver>basic_newton</nonlinear_solver>
                <convergence_criterion>
                    <type>PerComponentDeltaX</type>
                    <norm_type>NORM2</norm_type>
                    <reltols>1e-14 1e-14 1e-14</reltols>
                </convergence_criterion>
                <time_discretization>
                    <type>BackwardEuler</type>
                </time_discretization>
                <time_stepping>
                    <type>FixedTimeStepping</type>
                    <t_initial>0</t_initial>
                    <t_end>1e9</t_end>
                    <timesteps>
						<pair>
                            <repeat>1</repeat>
                            <delta_t>1e9</delta_t>
                        </pair>
                    </timesteps>
                </time_stepping>
            </process>
        </processes>
        <output>
            <type>VTK</type>
            <prefix>disc_with_hole_quasiisotropic</prefix>
            <timesteps>
                <pair>
                    <repeat>1</repeat>
                    <each_steps>1</each_steps>
                </pair>
            </timesteps>
            <variables>
                <variable>displacement</variable>
                <variable>pressure</variable>
                <variable>sigma_xx</variable>
                <variable>sigma_yy</variable>
                <variable>sigma_zz</variable>
                <variable>sigma_xy</variable>
                <variable>epsilon_xx</variable>
                <variable>epsilon_yy</variable>
                <variable>epsilon_zz</variable>
                <variable>epsilon_xy</variable>
                <variable>velocity</variable>
            </variables>
        </output>
    </time_loop>
    <parameters>
        <parameter>
            <name>phi</name>
            <type>RandomFieldMeshElement</type>
            <field_name>phi_xy</field_name>
            <range>0 3.1415926535</range>
            <seed>20210422</seed>
        </parameter>
        <parameter>
            <name>k</name>
            <type>Constant</type>
            <value>1e-18</value>
        </parameter>
        <parameter>
            <name>zero</name>
            <type>Constant</type>
            <value>0</value>
        </parameter>
        <parameter>
            <name>E</name>
            <type>Constant</type>
            <value>1e9</value>
        </parameter>
        <parameter>
            <name>nu</name>
            <type>Constant</type>
            <value>.3</value>
        </parameter>
        <parameter>
            <name>displacement0</name>
            <type>Constant</type>
            <values>0 0</values>
        </parameter>
        <parameter>
            <name>dirichlet0</name>
            <type>Constant</type>
            <value>0</value>
        </parameter>
        <parameter>
            <name>p_edge</name>
            <type>Constant</type>
			<value>0.1e6</value>
        </parameter>
        <parameter>
            <name>normal_traction_edge</name>
            <type>Constant</type>
			<value>0.1e6</value>
        </parameter>
        <parameter>
            <name>normal_traction_hole</name>
            <type>Constant</type>
			<value>-1e6</value>
        </parameter>
		<parameter>
            <name>pressure_ic</name>
            <type>Constant</type>
            <values>0.1e6</values>
        </parameter>
		<parameter>
            <name>p_hole</name>
            <type>Constant</type>
            <values>1e6</values>
        </parameter>
    </parameters>
    <process_variables>
        <process_variable>
            <name>displacement</name>
            <components>2</components>
            <order>1</order>
            <initial_condition>displacement0</initial_condition>
            <boundary_conditions>
                <boundary_condition>
                    <mesh>disc_with_hole_left</mesh>
                    <type>Dirichlet</type>
                    <component>0</component>
                    <parameter>dirichlet0</parameter>
                </boundary_condition>
                <boundary_condition>
                    <mesh>disc_with_hole_bottom</mesh>
                    <type>Dirichlet</type>
                    <component>1</component>
                    <parameter>dirichlet0</parameter>
                </boundary_condition>
                <boundary_condition>
                    <mesh>disc_with_hole_edge</mesh>
                    <type>NormalTraction</type>
                    <parameter>normal_traction_edge</parameter>
                </boundary_condition>
				<boundary_condition>
                    <mesh>disc_with_hole_hole</mesh>
                    <type>NormalTraction</type>
                    <parameter>normal_traction_hole</parameter>
                </boundary_condition>
            </boundary_conditions>
        </process_variable>
		<process_variable>
            <name>pressure</name>
            <components>1</components>
            <order>1</order>
            <initial_condition>pressure_ic</initial_condition>
            <boundary_conditions>
                <boundary_condition>
                    <mesh>disc_with_hole_hole</mesh>
                    <type>Dirichlet</type>
                    <component>0</component>
                    <parameter>p_hole</parameter>
                </boundary_condition>
                <boundary_condition>
                    <mesh>disc_with_hole_edge</mesh>
                    <type>Dirichlet</type>
                    <component>0</component>
                    <parameter>p_edge</parameter>
                </boundary_condition>
            </boundary_conditions>
        </process_variable>
    </process_variables>
    <nonlinear_solvers>
        <nonlinear_solver>
            <name>basic_newton</name>
            <type>Newton</type>
            <max_iter>50</max_iter>
            <linear_solver>general_linear_solver</linear_solver>
        </nonlinear_solver>
    </nonlinear_solvers>
    <linear_solvers>
        <linear_solver>
            <name>general_linear_solver</name>
            <lis>-i bicgstab -p ilu -tol 1e-12 -maxiter 10000</lis>
            <eigen>
                <solver_type>BiCGSTAB</solver_type>
                <precon_type>DIAGONAL</precon_type>
                <max_iteration_step>10000</max_iteration_step>
                <error_tolerance>1e-12</error_tolerance>
            </eigen>
        </linear_solver>
    </linear_solvers>
</OpenGeoSysProject>
