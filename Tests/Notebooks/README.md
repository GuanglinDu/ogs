# OGS Juypter notebook tips

## Git diff / merge tool

Install [nbdime](https://nbdime.readthedocs.io) and enable [git integration](https://nbdime.readthedocs.io/en/latest/vcs.html):

```bash
nbdime config-git --enable --global
```
